<?php
require dirname(__FILE__).'/setting.php';

class PDOConnect
{
    public $pdo;

    public function __construct()
    {
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
        $dotenv->load();

        $settings['db'] = [
            'driver' => $_ENV['DB_DRIVER'],
            'host' => $_ENV['DB_HOST'],
            'dbname' => $_ENV['DB_NAME'],
            'user' => $_ENV['DB_USER'],
            'pass' =>$_ENV['DB_PASS'],
            'socket' =>$_ENV['DB_SOCKET'],
        ];

        $dsn = $settings['db']['driver'] .
        ':host=' . $settings['db']['host'] .
        ';dbname=' . $settings['db']['dbname'] . 
        ';charset=utf8;unix_socket=' . $settings['db']['socket'];

        try{
            $pdo = new PDO($dsn, $settings['db']['user'], $settings['db']['pass']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            $this->pdo = $pdo;

        }catch (PDOException $e){
            print('Connection failed:'.$e->getMessage());
            die();
        }
       
    }
}