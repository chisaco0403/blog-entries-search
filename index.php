<?php
require dirname(__FILE__) .'/setting.php';
require dirname(__FILE__). '/query_function.php';

// cookieから検索項目を復元
$q = [];
if (isset($_COOKIE['q'])) {
    foreach ($_COOKIE['q'] as $key => $val) {
        $q[$key] = $val;
    }
}

// リクエストから検索項目を取得
$q = isset($_REQUEST['q']) ? array_merge($q, $_REQUEST['q']) : $q;

// SQLを生成
$select = 'SELECT * FROM entries ';
$selectCount = 'SELECT count(*) as count FROM entries ';
$whereQuery = QueryFunction::buildWhereQuery($q);

// 件数取得用SQL
$countSQL = $selectCount . $whereQuery;

// ページャー関連変数
$perPage = 100; // ページャー：1ページあたりの件数
$currentPage = isset($_REQUEST['p']) ? $_REQUEST['p'] : 1; // ページャー：現在のページ
$offset =  (($currentPage - 1) * $perPage); // ページャー：データ取得開始位置

// データ取得用SQL
$oderByQuery = 'ORDER BY create_date DESC, id DESC ';
$getSQL = $select . $whereQuery . $oderByQuery . "LIMIT ${perPage} OFFSET ${offset} ";

$dbc = new PDOConnect();

// 件数取得
$stmt = $dbc->pdo->prepare($countSQL);
$params = QueryFunction::buildParams($q);
$stmt->execute($params);
$countRow = $stmt->fetchall(PDO::FETCH_ASSOC);
$count = (int)$countRow[0]['count'];

// データ取得
$stmt = $dbc->pdo->prepare($getSQL);
$params = QueryFunction::buildParams($q);
$stmt->execute($params);
$entries = $stmt->fetchall(PDO::FETCH_ASSOC);

// ページャーの値を計算
$totalPage = ceil($count / $perPage); // 総ページ数
$prev = max(($currentPage - 1), 1); // 前のページ番号
$next = min(($currentPage + 1), $totalPage); // 次のページ番号
$pPrev = ($currentPage !== 1) ? $prev : null;
$pNext = ($currentPage < $totalPage) ? $next : null;
$countTo = ($count < ($currentPage * $perPage)) ? $count : $currentPage * $perPage;
$pStatus = (($offset === 0) ? 1 : $offset) . "件 - ${countTo}件（合計${count}件）";

// 画面表示の値をセット
$smarty = new Smarty();
$smarty->default_modifiers = ['escape:html', 'nl2br'];
$init_q = ['date' => '', 'url' => '', 'user-name' => '', 'server-no' => '', 'entry-no-from' => '', 'entry-no-to' => ''];
$q = $q + $init_q;

// 検索項目をcookieに保存
foreach ($q as $key => $val) {
    setcookie('q' . '[' . $key . ']', $val);
}

$smarty->assign('q', $q);
$smarty->assign('entries', $entries);

$getParams = $_GET;
unset($getParams['p']);
$defaultUrl = strtok($_SERVER["REQUEST_URI"], '?') .'?'. http_build_query($getParams);
$smarty->assign('defaultUrl', $defaultUrl);
$smarty->assign('pStatus', $pStatus);
$smarty->assign('pPrev', $pPrev);
$smarty->assign('pNext', $pNext);

$smarty->display('index.tpl');
