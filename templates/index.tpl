{extends file='layout.tpl'}
Ω
{block name=style}
    <style>
        .container-fluid .card .datetimepicker-input {
            margin-left: 15px;
            border-top-left-radius: .25rem;
            border-bottom-left-radius: .25rem;
        }
    </style>
{/block}

{block name=body}
    <div class="container-fluid">
        <div class="page-title-header pt-md-4 pb-md-2 mx-auto">
            <h3>FC2 BLOG 記事検索</h3>
        </div>
        <div class="card mb-md-4">
            <div class="card-body">
                <form method="get" action="">
                    <div class="form-group row">
                        <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                            <label for="q-date" class="col-sm-2 col-form-label">日付</label>
                            <input type="text" name="q[date]" value="{$q['date']}"
                                   class="form-control col-sm-2 datetimepicker-input" data-target="#datetimepicker"/>

                            <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="q-url" class="col-sm-2 col-form-label">URL</label>
                        <div class="col-sm-5">
                            <input type="text" name="q[url]" value="{$q['url']}" class="form-control" id="q-url">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="q-user-name" class="col-sm-2 col-form-label">ユーザー名</label>
                        <div class="col-sm-3">
                            <input type="text" name="q[user-name]" value="{$q['user-name']}" class="form-control"
                                   id="q-user-name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="q-server-no" class="col-sm-2 col-form-label">サーバー番号</label>
                        <div class="col-sm-2">
                            <input type="number" name="q[server-no]" value="{$q['server-no']}" class="form-control"
                                   id="q-server-no">
                        </div>
                        <label for="q-entry-no-from" class="col-sm-2 col-form-label">エントリーNo.</label>
                        <div class="col-sm-2">
                            <input type="number" name="q[entry-no-from]" value="{$q['entry-no-from']}"
                                   class="form-control"
                                   id="q-entry-no-from">
                        </div>
                        〜
                        <div class="col-sm-2">
                            <input type="number" name="q[entry-no-to]" value="{$q['entry-no-to']}" class="form-control"
                                   id="q-entry-no-to">
                        </div>
                        <input class="btn btn-primary" type="submit" value="検索">
                    </div>
                </form>
            </div>
        </div>
        {if count($entries) > 0}
            {include file="pager.tpl"}
            <table class="table table-hover ">
                <thead>
                <tr>
                    <th>日付</th>
                    <th>URL</th>
                    <th>タイトル</th>
                    <th>説明</th>
                </tr>
                </thead>
                <tbody>
                {foreach $entries as $entry}
                    <tr>
                        <th>{$entry['CREATE_DATE']}</th>
                        <td><a href="{$entry['URL']}" target="_blank">{$entry['URL']}</a></td>
                        <td>{$entry['TITLE']}</td>
                        <td><p>{$entry['DESCRIPTION']}</p></td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        {else}
            データが見つかりません。
        {/if}
    </div>
{/block}


{block name=script}
    {include file="script.tpl"}
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker').datetimepicker({
                locale: 'ja',
                dayViewHeaderFormat: 'YYYY年 M月',
                format: 'YYYY-MM-DD'
            });
        });
    </script>
{/block}

