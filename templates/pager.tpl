<div class="pager">
    {if $pPrev !== null}
        <a href="{$defaultUrl}&p={$pPrev}">戻る</a>
    {/if}
    <span>{$pStatus}</span>
    {if $pNext !== null}
        <a href="{$defaultUrl}&p={$pNext}">次へ</a>
    {/if}
</div>