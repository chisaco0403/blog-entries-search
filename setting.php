<?php
/**
 * 各種共通設定
 */

ini_set('display_errors', "On");
error_reporting(E_ALL);
date_default_timezone_set('Asia/Tokyo');

require dirname(__FILE__).'/vendor/autoload.php';
require_once dirname(__FILE__).'/pdo_connect.php';

