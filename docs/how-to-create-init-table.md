# テーブル作成手順

* mysql

````
# mysqlにログイン
mysql -u [ユーザー名] -p

# データベース指定
use [データベース名];

# テーブル作成DDL 実行

CREATE TABLE entries (
  ID    int(11) unsigned AUTO_INCREMENT NOT NULL,
  CREATE_DATE datetime NOT NULL,
  URL text NOT NULL,
  USER_NAME text,
  SERVER_NO  int(11),
  ENTRY_NO int(11),
  TITLE  text NOT NULL,
  DESCRIPTION  text,  
  PRIMARY KEY (ID)
);

# index作成DDL 実行
ALTER TABLE entries ADD INDEX CREATE_DATE_INDEX(CREATE_DATE);
ALTER TABLE entries ADD INDEX SERVER_NO_INDEX(SERVER_NO);
ALTER TABLE entries ADD INDEX ENTRY_NO_INDEX(ENTRY_NO);
````
