<?php
require dirname(__FILE__) .'/setting.php';

$dbc = new PDOConnect();

// TODO: バッチが終わってるのを確認してから、次のバッチを開始する（重複して起動しないようにする）

// 古いデータの削除
$sql = 'DELETE FROM entries WHERE create_date <= :target_date';
$stmt = $dbc->pdo->prepare($sql);
$d = new DateTime();
$d->modify('-2 weeks');
$targetDate = $d->format('Y-m-d H:i:s');
$stmt->execute([':target_date' => $targetDate]);
//TODO: エラー処理


// RSSのデータを取得
$RSS_URL = "https://blog.fc2.com/newentry.rdf";
$rssData = simplexml_load_file($RSS_URL, 'SimpleXMLElement', LIBXML_NOCDATA);
$rssArray = array();

foreach ($rssData->item as $item) {
    $link = $item->link;
    $title = $item->title;
    $description = $item->description;
    $date = (new DateTime((string)$item->children('dc', true)->date))->format('Y-m-d H:i:s');

    $pattern = '/http:\/\/(?<username>\w+).blog(?<serverno>\d*).*blog-entry-(?<entryno>\d+)\.html/';
    preg_match($pattern, $link, $match);
    $userName = (isset($match['username']) && !empty($match['username']))? $match['username'] : null;
    $serverNo = (isset($match['serverno']) && !empty($match['serverno']))? $match['serverno'] : null;
    $entryNo = (isset($match['entryno']) && !empty($match['entryno']))? $match['entryno'] : null;


    // 既に同じデータがDBに登録されてないかチェックする
    // TODO: データの存在確認をURLじゃなく別のものにしたほうがよさそう
    $stmt = $dbc->pdo->prepare('SELECT id FROM entries WHERE url=:url');
    $stmt->bindParam(':url', $link, PDO::PARAM_STR);
    $stmt->execute();
    $rows = $stmt->fetchall(PDO::FETCH_ASSOC);
    if(!empty($rows)) {
        continue; // データが既にあれば、何もしない
    }

    // データベースに保存
    $sql = 'INSERT INTO entries(' .
        'create_date, url, user_name, server_no, entry_no, title, description' .
        ') VALUE (' .
        ':create_date, :url, :user_name, :server_no, :entry_no, :title, :description)';
    $stmt = $dbc->pdo->prepare($sql);
    $stmt->bindValue(':create_date', $date, PDO::PARAM_STR);
    $stmt->bindValue(':url', $link, PDO::PARAM_STR);
    $stmt->bindValue(':user_name', $userName, PDO::PARAM_STR);
    $stmt->bindValue(':server_no', $serverNo, PDO::PARAM_STR);
    $stmt->bindValue(':entry_no', $entryNo, PDO::PARAM_STR);
    $stmt->bindValue(':title', $title, PDO::PARAM_STR);
    $stmt->bindValue(':description', $description, PDO::PARAM_STR);
    $stmt->execute(); //TODO: エラー処理
}

