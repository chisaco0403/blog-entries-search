<?php

class QueryFunction
{
    public static function buildWhereQuery($q)
    {
        $hasQuery = empty(array_filter($q)) ? false : true;

        $sql = '';
        if ($hasQuery) {
            $sql .= 'WHERE ';
        }

        $whereQuery = [];
        if (isset($q['date']) && !empty($q['date'])) {
            $whereQuery[] = 'create_date >= :dateFrom ';
            $whereQuery[] = 'create_date <= :dateTo ';
        }
        if (isset($q['url']) && !empty($q['url'])) {
            $whereQuery[] = 'url like :url ';
        }
        if (isset($q['user-name']) && !empty($q['user-name'])) {
            $whereQuery[] = 'user_name = :userName ';
        }
        if (isset($q['server-no']) && !empty($q['server-no'])) {
            $whereQuery[] = 'server_no = :serverNo ';
        }
        if (isset($q['entry-no-from']) && !empty($q['entry-no-from'])) {
            $whereQuery[] = 'entry_no >= :entryNoFrom ';
        }
        if (isset($q['entry-no-to']) && !empty($q['entry-no-to'])) {
            $whereQuery[] = 'entry_no <= :entryNoTo ';
        }

        if (isset($whereQuery)) {
            $sql .= implode(" AND ", $whereQuery);
        }

        return $sql;
    }

    public static function buildParams(array $q)
    {
        $params = [];
        if (isset($q['date']) && !empty($q['date'])) {
            $params += [':dateFrom' => $q['date'].' 00:00:00'];
            $params += [':dateTo' => $q['date']. ' 23:59:59'];
        }
        if (isset($q['url']) && !empty($q['url'])) {
            $params += [':url' => '%' . $q['url'] . '%'];
        }
        if (isset($q['user-name']) && !empty($q['user-name'])) {
            $params += [':userName' => $q['user-name']];
        }
        if (isset($q['server-no']) && !empty($q['server-no'])) {
            $params += [':serverNo' => $q['server-no']];
        }
        if (isset($q['entry-no-from']) && !empty($q['entry-no-from'])) {
            $params += [':entryNoFrom' => $q['entry-no-from']];
        }
        if (isset($q['entry-no-to']) && !empty($q['entry-no-to'])) {
            $params += [':entryNoTo' => $q['entry-no-to']];
        }
        return $params;
    }
}