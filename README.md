#blog-entries-search

## 説明
このアプリは、大きく２つの機能があります。
1. RSSのブログ記事データを取得して、DBに保存する機能
2. 保存したブログ記事データを検索・閲覧機能

## 必要なライブラリ等
PHP7.1以上, MySQL, composer

## 初期テーブルセットアップ方法
docs/how-to-create-init-table.md を見てください。

## アプリをデプロイする手順
1. サーバーにアクセスし、ドキュメントルートに移動

2. git cloneしてソース取得（事前にbitbucketにサーバーのdeployキー設定が必要）

3. composerで必要なライブラリインストール
```
./composer.phar install
```

4. ディレクトリの権限設定
```
chmod 777 templates_c
```

5. .envファイルの生成
.env.example を コピーして、.env を作り、中身を環境に合わせて修正する。

## cronにバッチを登録する
```
crontab -e
*/5 * * * * [path to php]/php [path to batch]/get_entries.php > /tmp/get_entries.log 2>&1
```